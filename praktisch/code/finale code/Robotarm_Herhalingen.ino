//#include <OLED_I2C.h>


#include "AccelStepper.h"
#include <Servo.h>
// Library created by Mike McCauley at http://www.airspayce.com/mikem/arduino/AccelStepper/

// AccelStepper Setup

// Nano Pin 2 connected to STEP pin of Easy Driver
// Nano Pin 3 connected to DIR pin of Easy Driver

AccelStepper stepper1(1, 4, 5);

AccelStepper stepper2(1, 6, 7);


Servo servo1;
Servo servo2;
Servo servo3;
// Variables to store current, previous and move position

//OLED  myOLED(SDA, SCL);
//extern uint8_t SmallFont[];

//define the buttons
//const int button1 = 8;
const int  Up_buttonPin1   = 8;    // the pin that the pushbutton is attached to
int up_buttonState1 = 0;         // current state of the up button
int up_lastButtonState1 = 0;     // previous state of the up button

//const int button2 = 9;
const int  Up_buttonPin2   = 2;    // the pin that the pushbutton is attached to
int up_buttonState2 = 0;         // current state of the up button
int up_lastButtonState2 = 0;     // previous state of the up button

//const int buttonEN = 10;
const int  Up_buttonPin3  = 3;    // the pin that the pushbutton is attached to
//int up_buttonState3 = 0;         // current state of the up button
//int up_lastButtonState3 = 0;     // previous state of the up button

/*bool bPress = false;
bool bPress2 = false;
bool bPress3 = false;*/
int buttonPushCounter;


//define variable for values of the button
//int button1Pressed = 0;
boolean button2Pressed = false;
boolean button3Pressed = false;
//define potentiometers
const int pot1 = A1;
const int pot2 = A2;
const int pot3 = A3;
const int pot4 = A4;
const int pot5 = A5;


int led1 = 12;
int led2 = 13;


//define variable for values of the potentiometers
int pot1Val;
int pot2Val;
int pot3Val;
int pot4Val;
int pot5Val;


//int pot4Val;

//define variable for angles of the potentiometer
int pot1Angle;
int pot2Angle;
int pot3Angle;
int pot4Angle;
int pot5Angle;

//int pot4Angle;

//define variable for saved position of the servos

int stepper1PosSave[] = {1, 1, 1, 1, 1, 1, 1, 1};
int stepper2PosSave[] = {1, 1, 1, 1, 1, 1, 1, 1};
int servo3PosSave[] = {1, 1, 1, 1, 1, 1, 1, 1};
int servo4PosSave[] = {1, 1, 1, 1, 1, 1, 1, 1};
int servo5PosSave[] = {1, 1, 1, 1, 1, 1, 1, 1};


void setup() {

  Serial.begin(9600);
  stepper1.setMaxSpeed(2000);  // Set speed fast enough to follow pot rotation
  stepper1.setAcceleration(2000);  //  High Acceleration to follow pot rotation
  stepper2.setMaxSpeed(2000);  // Set speed fast enough to follow pot rotation
  stepper2.setAcceleration(2000);  //  High Acceleration to follow pot rotation

 // stepper1.moveTo(0);////////////////////////////////////////////////////////////////////////////////////////
 // stepper2.moveTo(0);//////////////////////////////////////////////////////////////////////////////////////

  /* pot1Angle=1600;
    pot2Angle=1600;
    pot3Angle=1600;*/
  servo1.attach(9);
  servo2.attach(10);
  servo3.attach(11);
  //define buttons as input units
  pinMode( Up_buttonPin1 , INPUT);
  pinMode( Up_buttonPin2 , INPUT);
  pinMode( Up_buttonPin3 , INPUT);
  pinMode( led1 , OUTPUT);
  pinMode( led2 , OUTPUT);
  attachInterrupt(digitalPinToInterrupt(Up_buttonPin3), buttonStop, CHANGE);
}

void loop() {

  pot1Val = analogRead(pot1);
  pot1Angle = map (pot1Val, 0, 1023, 0, 1600);
  pot2Val = analogRead(pot2);
  pot2Angle = map (pot2Val, 0, 1023, 0, 1600);
  pot3Val = analogRead(pot3);
  pot3Angle = map (pot3Val, 0, 1023, 0, 1600);
  pot4Val = analogRead(pot4);
  pot4Angle = map (pot4Val, 0, 1023, 0, 180);
  pot5Val = analogRead(pot5);
  pot5Angle = map (pot5Val, 0, 1023, 0, 180);
  

  stepper1.runToNewPosition(pot1Angle);            //???????????????
  stepper2.runToNewPosition(pot2Angle);            //?????????????
  servo1.write(pot3Angle);
  servo2.write(pot4Angle);
  servo3.write(pot5Angle);

  //if button1 is pressed (HIGH), save the potentiometers position
  //as long as button1 is pressed
  up_buttonState1 = digitalRead(Up_buttonPin1);

  if (up_buttonState1 != up_lastButtonState1) {

    if (up_buttonState1 == LOW) {
      /*bPress = true;
      if (bPress == true) {*/
        buttonPushCounter++;
        digitalWrite(led1, HIGH);
      /*} else {
        //Serial.println("off");

      }*/
      delay(50);
      digitalWrite(led1, LOW);
    }
    up_lastButtonState1 = up_buttonState1;

    if(buttonPushCounter==8){             ///////////////////////////////////////
    Serial.println("8 keer ingeduwd");
      digitalWrite(led1,HIGH);
      button2Pressed=true;
      }

    switch ( buttonPushCounter) {

      case 1:
      Serial.println("eerste save");
        stepper1PosSave[0] = pot1Angle;
        stepper2PosSave[0] = pot2Angle;
        servo3PosSave[0] = pot3Angle;
        servo4PosSave[0] = pot4Angle;
        servo5PosSave[0] = pot5Angle;
        break;
      case 2:
      Serial.println("tweede save");
        stepper1PosSave[1] = pot1Angle;
        stepper2PosSave[1] = pot2Angle;
        servo3PosSave[1] = pot3Angle;
        servo4PosSave[1] = pot4Angle;
        servo5PosSave[1] = pot5Angle;
        break;
      case 3:
      Serial.println("derde save");
        stepper1PosSave[2] = pot1Angle;
        stepper2PosSave[2] = pot2Angle;
        servo3PosSave[2] = pot3Angle;
        servo4PosSave[2] = pot4Angle;
        servo5PosSave[2] = pot5Angle;
        break;
      case 4:
      Serial.println("vierde save");
        stepper1PosSave[3] = pot1Angle;
        stepper2PosSave[3] = pot2Angle;
        servo3PosSave[3] = pot3Angle;
        servo4PosSave[3] = pot4Angle;
        servo5PosSave[3] = pot5Angle;
        break;
      case 5:
      Serial.println("vijfde save");
        stepper1PosSave[4] = pot1Angle;
        stepper2PosSave[4] = pot2Angle;
        servo3PosSave[4] = pot3Angle;
        servo4PosSave[4] = pot4Angle;
        servo5PosSave[4] = pot5Angle;
        break;
      case 6:
      Serial.println("zesde save");
        stepper1PosSave[5] = pot1Angle;
        stepper2PosSave[5] = pot2Angle;
        servo3PosSave[5] = pot3Angle;
        servo4PosSave[5] = pot4Angle;
        servo5PosSave[5] = pot5Angle;
        break;
      case 7:
      Serial.println("zevende save");
        stepper1PosSave[6] = pot1Angle;
        stepper2PosSave[6] = pot2Angle;
        servo3PosSave[6] = pot3Angle;
        servo4PosSave[6] = pot4Angle;
        servo5PosSave[6] = pot5Angle;
        break;
      case 8:
      Serial.println("achtste save");
        stepper1PosSave[7] = pot1Angle;
        stepper2PosSave[7] = pot2Angle;
        servo3PosSave[7] = pot3Angle;
        servo4PosSave[7] = pot4Angle;
        servo5PosSave[7] = pot5Angle;
        break;
    }

  }

  up_buttonState2 = digitalRead(Up_buttonPin2);

  if (up_buttonState2 != up_lastButtonState2) {

    if (up_buttonState2 == LOW) {
      Serial.println("start knop ingeduwd");
    /*  bPress2 = true;
      if (bPress2 = true) {*/
        button2Pressed = true;
      /*} else {
        // Serial.println("off");
      }*/
      delay(50);
    }
    up_lastButtonState2 = up_buttonState2;
  }



  if (button2Pressed) {

    digitalWrite(led2, HIGH);
    Serial.println("loop voor posities worden uitgevoerd");
    for (int i = 0; i < 8; i++) {

    /*  up_buttonState3 = digitalRead(Up_buttonPin3);

      if (up_buttonState3 != up_lastButtonState3) {

        if (up_buttonState3 == LOW) {
          bPress3 = true;
          if (bPress3 = true) {
            button3Pressed = true;
          } else {
            // Serial.println("off");
          }
          delay(50);
        }
        up_lastButtonState3 = up_buttonState3;
      }
*/

      /*if (button3Pressed) {
        buttonPushCounter = 0;
        button2Pressed = false;
        digitalWrite(led1, LOW);
        digitalWrite(led2, LOW);
        break;
        }*/
 
      stepper1.runToNewPosition(stepper1PosSave[i]);
      stepper2.runToNewPosition(stepper2PosSave[i]);
      servo1.write( servo3PosSave[i]);
      servo2.write( servo4PosSave[i]);
      servo3.write( servo5PosSave[i]);
    }
  }
}

void buttonStop() {
  buttonPushCounter = 0;
  button2Pressed = false;
  Serial.println("interrupt knop ingeduwd");
  digitalWrite(led1, LOW);
  digitalWrite(led2, LOW);
}