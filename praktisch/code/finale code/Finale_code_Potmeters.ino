#include <Servo.h>
#include "AccelStepper.h"

// AccelStepper Setup
#define stepPin 6
#define dirPin 7
#define potPin A1

#define stepPin1 4
#define dirPin1 5
#define potPin1 A2

// Vervang de vaste vertraging door variabelen voor de timing
unsigned long previousMillis = 0;
const long interval = 15;

AccelStepper stepper(1, stepPin, dirPin); // 1 is voor driver mode
AccelStepper stepper1(1, stepPin1, dirPin1);

Servo myservoDraaien;
Servo myservoGrijper;
Servo myservoOpEnNeer;  

const int potpinOpEnNeer = A3;
const int potpingrijper = A4;
const int potpindraaien = A5;  
const int servoPinDraaien = 9;
const int servoPinGrijper = 10;
const int servoPinOpEnNeer = 11;

int valDraaien;
int valGrijper;
int valOpEnNeer;    
int minPulseWidth = 600;  // Minimum puls breedte in microseconden
int maxPulseWidth = 2550; // Maximum puls breedte in microseconden
int minPulseWidth1 = 400;  // Minimum puls breedte in microseconden
int maxPulseWidth1 = 1600;
int minPulseWidth2 = 600;  // Minimum puls breedte in microseconden
int maxPulseWidth2 = 2000;  // Maximum puls breedte in microseconden

int previousValDraaien = 0;
int previousValGrijper = 0;
int previousValOpEnNeer = 0; 
int threshold = 10; 
const int deadzone = 15;
int servoAngleGrijper;
int servoAngleDraaien;
int servoAngleOpEnNeer;

void setup() {

  stepper.setMaxSpeed(4800);
  stepper.setAcceleration(4800);
  stepper1.setMaxSpeed(4800);
  stepper1.setAcceleration(4800);
    
  myservoOpEnNeer.attach(servoPinOpEnNeer, minPulseWidth2, maxPulseWidth2); 
  myservoGrijper.attach(servoPinGrijper, minPulseWidth1, maxPulseWidth1); 
  myservoDraaien.attach(servoPinDraaien, minPulseWidth, maxPulseWidth); 

  Serial.begin(9600);
}

void loop() {
  int potValue = analogRead(potPin); // Leest de waarde van de potentiometer
  int potValue1 = analogRead(potPin1);
  long targetPosition = map(potValue, 0, 1023, 0, 1600); // Converteert de potentiometerwaarde naar een doelpositie
  long targetPosition1 = map(potValue1, 0, 1023, 0, 1600);

  stepper.moveTo(targetPosition); // Zet de doelpositie
  stepper1.moveTo(targetPosition1);

  if (stepper1.distanceToGo() != 0){
    stepper1.run();
  }

  if (stepper.distanceToGo() != 0) {
    stepper.run(); // Beweegt de stepper naar de doelpositie
  }

  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis >= interval) {
    previousMillis = currentMillis;

  valDraaien = analogRead(potpindraaien);           
  servoAngleDraaien = map(valDraaien, 0, 1023, 0, 180);    // schaal het naar gebruik met de servo (waarde tussen 0 en 180)
  
  if(valDraaien < 512 -deadzone || valDraaien > 512 + deadzone) { 
    myservoDraaien.writeMicroseconds(map(servoAngleDraaien, 0, 180, 600, 2450));
  }


  valGrijper = analogRead(potpingrijper);           
  servoAngleGrijper = map(valGrijper, 0, 1023, 0, 180);    // schaal het naar gebruik met de servo (waarde tussen 0 en 180)
  
  if(valGrijper < 512 -deadzone || valGrijper > 512 + deadzone) { 
    myservoGrijper.writeMicroseconds(map(servoAngleGrijper, 0, 180, 700, 1600));
  }


  valOpEnNeer = analogRead(potpinOpEnNeer);           
  servoAngleOpEnNeer = map(valOpEnNeer, 0, 1023, 0, 180);    // schaal het naar gebruik met de servo (waarde tussen 0 en 180)
  
  if(valOpEnNeer < 512 -deadzone || valOpEnNeer > 512 + deadzone) { 
    myservoOpEnNeer.writeMicroseconds(map(servoAngleOpEnNeer, 0, 180, 800, 1800));
  }
  }
}

