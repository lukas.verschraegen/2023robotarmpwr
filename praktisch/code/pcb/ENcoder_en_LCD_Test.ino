// Rotary Encoder Inputs
#define encoderCLK 3   //D1
#define encoderDT 4    //D2
#define encoderSW 6    // D3

#include <Arduino.h>
#include <U8g2lib.h>

#ifdef U8X8_HAVE_HW_SPI
#include <SPI.h>
#endif
#ifdef U8X8_HAVE_HW_I2C
#include <Wire.h>
#endif

U8G2_ST7920_128X64_F_SW_SPI u8g2(U8G2_R2, /* clock=*/ 12, /* data=*/ 11, /* CS=*/ 10, /* reset=*/ 45); 

int servoAngle = 0; 
int savedServoAngle = 0; // Variable to save the servo angle
int crntCLK;
int prvsCLK;
int crntSWstate;
int prvsSWstate = HIGH;
unsigned long lastDebounceTime = 0;  // the last time the output pin was toggled
unsigned long debounceDelay = 50;    // the debounce time; increase if the output flickers

void setup() { 
   pinMode (encoderCLK,INPUT);
   pinMode (encoderDT,INPUT);
   pinMode (encoderSW, INPUT);

   u8g2.begin();
   
   prvsCLK = digitalRead(encoderCLK);
} 
 
void loop() {
   crntCLK = digitalRead(encoderCLK);
   int reading = digitalRead(encoderSW);

   // If the switch changed, due to noise or pressing
   if (reading != prvsSWstate) {
     // Reset the debouncing timer
     lastDebounceTime = millis();
   }

   if ((millis() - lastDebounceTime) > debounceDelay) {
     // Whatever the reading is at, it's been there for longer than the debounce
     // delay, so take it as the actual current state:

     // If the button state has changed:
     if (reading != crntSWstate) {
       crntSWstate = reading;

       // Only toggle the LED if the new button state is LOW
       if (crntSWstate == LOW) {
         savedServoAngle = servoAngle;
       }
     }
   }

   if (crntCLK != prvsCLK){
      if (digitalRead(encoderDT) != crntCLK) {
         servoAngle --;

         if (servoAngle<0){
            servoAngle=0;
         }
      } 
      else {
         servoAngle ++;
         if (servoAngle>180){
            servoAngle= 180;
         }
      }

      u8g2.clearBuffer();
      u8g2.setFont(u8g2_font_ncenB08_tr);
      String str = "Servo Angle: " + String(servoAngle);
      u8g2.drawStr(0,10,str.c_str());

      String savedStr = "Saved Angle: " + String(savedServoAngle);
      u8g2.drawStr(0,25,savedStr.c_str());

      u8g2.sendBuffer();				
    } 
    prvsCLK = crntCLK; 
    prvsSWstate = reading; // Set the last reading from the input pin
}

