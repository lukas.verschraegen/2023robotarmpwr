const int potPin2 = 14; 
const int potPin4 = 17; // Analog input pin connected to the potentiometer
const int potPin6 = 7;

void setup() {
  Serial.begin(9600);
  pinMode(potPin2, INPUT);
  pinMode(potPin4,INPUT);
  pinMode(potPin6, INPUT);  // Initialize serial communication for debugging
}

void loop() {
  // Read the raw analog value from the potentiometer
  int potValue2 = analogRead(potPin2);
  int potValue4 = analogRead(potPin4);
  int potValue6 = analogRead(potPin6);

  // Map the raw value to a range between 0 and 1023
  int mappedValue2 = map(potValue2, 0, 4095, 0, 1023);
  int mappedValue4 = map(potValue4, 0, 4095, 0, 1023);
  int mappedValue6 = map(potValue6, 0, 4095, 0, 1023);
  // Print the mapped value to the serial monitor
  Serial.print("Pot 1: ");
  Serial.print(mappedValue2);
  Serial.print(" Pot 2: ");
  Serial.print(mappedValue4);
  Serial.print(" Pot 3: ");
  Serial.println(mappedValue6);
  
  delay(100);  // Delay for stability or adjust as per your requirements
}