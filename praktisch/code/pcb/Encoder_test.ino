// Include the Servo Library

 
 // Rotary Encoder Inputs
 #define encoderCLK 3   //D1
 #define encoderDT 4    //D2
 #define encoderSW 6    // D3


 
 int servoAngle = 0; 
 int crntCLK;
 int prvsCLK;
 int SWstate = 0;
 
 void setup() { 
   
   // Set LEDs and rotary encoder pins as inputs  
   pinMode (encoderCLK,INPUT);
   pinMode (encoderDT,INPUT);
   pinMode (encoderSW, INPUT);



   // Setup Serial Monitor to see rotation angle
   Serial.begin (9600);
   
   // Read the initial state of encoderCLK and assign it to prvsCLK variable
   prvsCLK = digitalRead(encoderCLK);
    
 } 
 
 void loop() {
  
   // Read the current state of encoderCLK
   crntCLK = digitalRead(encoderCLK);
   // Read the state of the encoder's pushbutton value
   SWstate = digitalRead(encoderSW);

   // Lights the yellow LED to indicate increasing rotation speed
  /* if (SWstate == LOW){
   Serial.println("true");

   } else{
    Serial.println("false");

   }
    */
   // A pulse occurs if the previous and the current state differ
    if (crntCLK != prvsCLK){
      // If the encoderDT state is different than the encoderCLK state then the rotary encoder is rotating counterclockwise
        if (digitalRead(encoderDT) != crntCLK) {
          if (SWstate == HIGH){
            servoAngle --;
          }else{
            servoAngle = servoAngle - 10;
          }

          if (servoAngle<0){
            servoAngle=-0;
          }
        } 
        else {
          // Encoder is rotating clockwise
          if (SWstate == HIGH){
            servoAngle ++;
          }else{
            servoAngle = servoAngle + 10;
          }
        if (servoAngle>180){
          servoAngle= 180;
        }
      }
     
      Serial.println(servoAngle);
    } 
    // Update prvsCLK with the current state
    prvsCLK = crntCLK; 
 }
