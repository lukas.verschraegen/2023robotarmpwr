# 2023robotarmpwr

In dit project is gewerkt aan de opbouw van een robotarm, waarbij gebruik is gemaakt van 
drie stappenmotoren en drie servomotoren. Voor de stappenmotoren was het noodzakelijk 
om een driver te gebruiken, waardoor een precieze en gecontroleerde beweging van de arm 
mogelijk werd gemaakt. 

Helaas deden zich problemen voor bij het verbinden van deze motoren, waardoor er een 
back-upplan moest worden opgesteld. Desondanks is er een eigen 3D-print van de robotarm 
gemaakt en werden er aparte codes geschreven voor het programma op een ESP32-S2 en 
het back-upplan op een Arduino Uno.

Het cruciale 3D-ontwerp van de robotarm bood de mogelijkheid om de arm nauwkeurig en 
efficiënt te construeren, met de vrijheid om aanpassingen te maken die aansluiten bij het 
specifieke doel. Het hoofddoel van het project was om de robotarm zelfstandig een cyclus te 
laten doorlopen nadat de posities waren bepaald. Ondanks enkele uitdagingen is er 
uiteindelijk een werkend prototype gecreëerd.
